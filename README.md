# Laravel AzAuth

Package to manage authorization on microservices

### Installation

`composer require altra/azauth`
`php artisan vendor:publish` - Config and user factory

### Usage

#### Middleware

```php
Route::middleware('AzAuth')->get();
```

#### Fake user

```php
use Altra\AzAuth\Facades\AzAuth;
use Altra\AzAuth\User; // Or other Authenticatable model class

AzAuth::fake(new User(['id' => 1, 'name' => 'Altra User']));
```

#### Gates for checking abilities based on user permissions

Return true or false based on user permissions

```php
auth()->user()->can('view-model');
```
