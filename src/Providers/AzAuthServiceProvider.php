<?php

namespace Altra\AzAuth\Providers;

use Altra\AzAuth\Middleware\AzAuth;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AzAuthServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../../config/az_auth.php' => config_path('az_auth.php'),
        ]);

        if ($this->app) {
            $router = $this->app->make(Router::class);
            $router->aliasMiddleware('AzAuth', AzAuth::class);
        }
        $this->publishes([
            __DIR__.'/../../database/factories/' => database_path('factories'),
        ]);

        Gate::before(function ($user, $ability) {
            return in_array($ability, collect($user->permissions)->pluck('name')->toArray());
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
