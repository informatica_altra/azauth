<?php

namespace Altra\AzAuth\Facades;

use Altra\AzAuth\Services\AuthService;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Facades\Http;

class AzAuth extends Facade
{
    public static function getFacadeAccessor()
    {
        return AuthService::class;
    }

    public static function fake(Authenticatable $user)
    {
        $endpoint = config('az_auth.loginservice_url').'/auth/user';

        Http::fake([
            $endpoint => Http::response(['body' => ['user' => $user->toArray()]], 200, []),
        ]);
    }
}
