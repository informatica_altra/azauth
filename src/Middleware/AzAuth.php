<?php

namespace Altra\AzAuth\Middleware;

use Altra\AzAuth\Facades\AzAuth as AzAuthFacade;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class AzAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $token = $request->bearerToken();
        try {
            $response = Cache::remember('user-'.$token, now()->tomorrow(), function () use ($token) {
                return AzAuthFacade::getUserByToken($token);
            });

            if (! isset($response['body'])) {
                return response()->error(__('loginservice.curlError'), 503, $response);
            }

            $userClass = config('az_auth.user_model');
            $user = new $userClass($response['body']['user']);
            Auth::login($user);
        } catch (\Throwable$th) {
            return response()->error($th->getMessage());
        }

        return $next($request);
    }
}
