<?php

namespace Altra\AzAuth\Services;

use Illuminate\Support\Facades\Http;

class AuthService
{
    public function getUserByToken(string|null $token)
    {
        $headers = array_merge(config('az_auth.headers'), ['Authorization' => 'Bearer '.$token]);
        $endpoint = config('az_auth.loginservice_url').'/auth/user';

        return Http::withHeaders($headers)->get($endpoint)->json();
    }
}
