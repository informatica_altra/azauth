<?php

use Altra\AzAuth\User;

return [
    'loginservice_url' => env('LOGINSERVICE_URL'),
    'headers' => [
        'apikey' => env('KONG_API_KEY'),
        'authentication_header' => env('KONG_AUTHENTICATION_HEADER'),
        'Accept' => 'application/json',
        'Content-type' => 'application/json',
    ],
    // User model MUST extends from Illuminate\Foundation\Auth\User
    'user_model' => User::class,
];
