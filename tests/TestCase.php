<?php

namespace Altra\AzAuth\Tests;

use Altra\AzAuth\Providers\AzAuthServiceProvider;
use Altra\AzAuth\User;
use Orchestra\Testbench\TestCase as Orchestra;

class TestCase extends Orchestra
{
    protected function setUp(): void
    {
        parent::setUp();

        config(['az_auth.loginservice_url' => 'test']);
        config(['az_auth.headers' => [
            'apikey' => 'test',
            'authentication_header' => 'test',
            'Accept' => 'application/json',
            'Content-type' => 'application/json',
        ]]);
        config(['az_auth.user_model' => User::class]);
        $this->createDummyprovider()->boot();
    }

    /**
     * @throws \ReflectionException
     */
    protected function createDummyprovider(): object
    {
        $reflectionClass = new \ReflectionClass(AzAuthServiceProvider::class);

        return $reflectionClass->newInstanceWithoutConstructor();
    }
}
