<?php

use Altra\AzAuth\Facades\AzAuth;
use Altra\AzAuth\Middleware\AzAuth as MiddlewareAzAuth;
use Altra\AzAuth\Tests\TestCase;
use Altra\AzAuth\User;

class AzAuthTest extends TestCase
{
    public function test_fake_middleware()
    {
        AzAuth::fake(new User(['id' => 45, 'name' => 'Marco']));

        $middleware = new MiddlewareAzAuth();
        $middleware->handle(request(), fn () => '');

        $this->assertEquals(45, auth()->id());
        $this->assertEquals('Marco', auth()->user()->name);
    }

    public function test_fake_middleware_permissions()
    {
        AzAuth::fake(new User([
            'id' => 45,
            'name' => 'Marco',
            'permissions' => [['name' => 'test-perm']],
        ]));

        $middleware = new MiddlewareAzAuth();
        $middleware->handle(request(), fn () => '');

        $this->assertTrue(auth()->user()->can('test-perm'));
        $this->assertFalse(auth()->user()->can('test-false'));
    }
}
